Ansible role for dokos
==========================

This role is not packaged, as it's very specific to Distrilab ansible context, but hopefully it may serve as an inspiration for other people that want to install dokos 3.x.x with ansible in their context.

This follow the manual; procedure initially tested and detailed on:

- https://md.distrilab.fr/xQu1bFRnSIWDT2qv5unzxw?both

